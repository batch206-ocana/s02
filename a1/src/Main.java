import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[] fruits = {"Apple","Avocado","Banana","Kiwi","Orange"};
        System.out.println(Arrays.toString(fruits));

        Scanner scanner = new Scanner(System.in);

        System.out.println("Which fruit would you like to get the index of?");
        String fruitTerm = scanner.nextLine();
        int fruitResult = Arrays.binarySearch(fruits, fruitTerm);
        System.out.println("The index of " +fruitTerm +" is " +fruitResult);

        ArrayList<String> friends = new ArrayList<>();
        friends.add("Kevin");
        friends.add("Bob");
        friends.add("Stuart");
        friends.add("Mary");
        System.out.println("My friends are: " +friends);

        HashMap<String,Integer> inventory = new HashMap<>();
        inventory.put("toothpaste",15);
        inventory.put("toothbrush",20);
        inventory.put("soap",12);
        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);
    }
}