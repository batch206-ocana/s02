import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //Operators in Java
            //Arithmetic: + - * / %
            //Comparison: > < >= <= == !=
            //Logical: && || !
            //Assignment: =

        int num1 = 36;

        if(num1 % 5 == 0){
            System.out.println(num1 +" is divisible by 5");
        } else {
            System.out.println(num1 +" is not divisible by 5");
        }

        //Switch Cases
            /*Switch statements are control flow structures that allow one code block to be run out of many other code blocks.
            * We compare the given value against each case and if a case matches we will run that code block.
            * This is mostly used if the user input is predictable
            * */

            /*Scanner scanner = new Scanner(System.in);

            System.out.println("Enter a number from 1-4:");
            int directionValue = scanner.nextInt();

            switch (directionValue){
                case 1:
                    System.out.println("SM North Edsa");
                    break;
                case 2:
                    System.out.println("SM Southmall");
                    break;
                case 3:
                    System.out.println("SM City Taytay");
                    break;
                case 4:
                    System.out.println("SM Manila");
                    break;
                default:
                    System.out.println("Out of Range");
            }*/

        //Data Collection (Array)
            //datatype[] identifier = new dataType[numberOfElements];
            String[] names = new String[3];
            names[0] = "Clark";
            names[1] = "Diana";
            names[2] = "Bruce";
            Arrays.sort(names);
            System.out.println(Arrays.toString(names)); // This will convert the array into string

            //datatype[] identifier = {element0, element1, ...};
            int[] numbers = {45,12,48,6,53,1};

                //sort() method
                System.out.println(Arrays.toString(numbers));
                Arrays.sort(numbers);
                System.out.println(Arrays.toString(numbers));

                //binarySearch() method: allows us to search for an element in an array
                String searchTerm = "Bruce";
                int result = Arrays.binarySearch(names, searchTerm);
                System.out.println("The index of " +searchTerm +" is " +result);

        //Data Collection (ArrayList): resizable
            ArrayList<String> students = new ArrayList<>();
            students.add("Paul");
            students.add("Sam");
            System.out.println(students);
                //arrayListName.get(index) - retrieve item using index
                System.out.println(students.get(1));

                //arrayListName.set(index, value) - update an item by its index
                students.set(0,"George");
                System.out.println(students);

                //arrayListName.remove(index) - remove an item using index
                students.remove(1);
                System.out.println(students);

                //arrayListName.clear() - clears all item in the array list
                students.add("Ringo");
                students.clear();
                System.out.println(students);

                //arrayListName.size() - returns the length of the array list
                students.add("James");
                students.add("Franco");
                System.out.println(students.size());

            //ArrayList with Initialized Values
            ArrayList<String> employees = new ArrayList<>(Arrays.asList("Bill Gates","Elon Musk","Jeff Bezos"));
            System.out.println(employees);
            employees.add("Lucio Tan");
            System.out.println(employees);

        //HashMaps
        /*
        * Most objects in java are defined and are instantiations of classes that contain a proper set of properties and methods.There might be used cases where this is not appropriate or you may simply want to sore a collection of data in key-value pairs.
        * In Java, "keys" also referred as "fields"
        * Hashmaps offers flexibility when storing a collection of data
        * HashMap<fieldDataType, valueDataType> identifier = new HashMap<>();
        * */

        HashMap<String, String> userRoles = new HashMap<>();
            //hashMapName.put(<field>,<value>); | Add new field and values in the hashmap
            userRoles.put("Anna","Admin");
            userRoles.put("Alice","User");
            userRoles.put("Alice","Teacher");
            userRoles.put("Dennis","User");
            System.out.println(userRoles);

            //hashMapName.get("field") : retrieve data
            System.out.println(userRoles.get("Anna"));

            //hashMapName.remove("field") : remove data
            userRoles.remove("Dennis");
            System.out.println(userRoles);

            //hashMapName.keyset() : retrieve hashmaps keys
            System.out.println(userRoles.keySet());

    }
}